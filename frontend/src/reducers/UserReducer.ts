import {
    loginRoutine,
    SET_CURR_USER,
    userEditRoutine,
    userFetchRoutine,
    usersFetchRoutine
} from "../actions/ActionTypes";

const initialState = {
    isAuthorized: !!localStorage.getItem('currUser'),
    // @ts-ignore
    currUser: JSON.parse(localStorage.getItem('currUser')),
    users: [],
    fetchedUser: {}
}

export default (state = initialState, action: any) => {
    switch (action.type) {
        case SET_CURR_USER: {
            localStorage.setItem('currUser', JSON.stringify(action.payload));
            return {
                ...state,
                currUser: action.payload
            }
        }
        case loginRoutine.SUCCESS:
            return {
                ...state,
                isAuthorized: true
            }
        case usersFetchRoutine.SUCCESS:
            return {
                ...state,
                users: action.payload
            }
        case userFetchRoutine.SUCCESS:
            return {
                ...state,
                fetchedUser: action.payload
            }
        case userEditRoutine.SUCCESS:
            return {
                ...state,
                fetchedUser: {}
            }
        default:
            return state;
    }
}
