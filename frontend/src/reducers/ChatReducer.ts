import {
    loading,
    messageEditRoutine,
    messageFetchRoutine,
    messageSendRoutine,
    messagesFetchRoutine,
    prevMessageRoutine
} from "../actions/ActionTypes";

const initialState = {
    messages: [],
    prevMessage: undefined,
    isLoadingError: false,
    isLoading: false,
    title: 'My chat',
    isMessageSending: false,
    fetchedMessage: {}
}

export default (state = initialState, action: any) => {
    switch (action.type) {
        case messagesFetchRoutine.SUCCESS: {
            return {
                ...state,
                messages: action.payload.messages
            }
        }
        case messageSendRoutine.TRIGGER:
            return {
                ...state,
                isMessageSending: true
            }
        case messageSendRoutine.SUCCESS:
            return {
                ...state,
                isMessageSending: false,
                prevMessage: action.payload
            }
        case messageEditRoutine.SUCCESS:
            return {
                ...state,
                fetchedMessage: {}
            }
        case prevMessageRoutine.SUCCESS:
            return {
                ...state,
                prevMessage: action.payload
            }
        case loading.SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoadingError: false
            }
        case loading.FAILURE:
            return {
                ...state,
                isLoadingError: true,
                isLoading: false
            }
        case loading.TRIGGER:
            return {
                ...state,
                isLoading: true,
                isLoadingError: false
            }
        case messageFetchRoutine.SUCCESS:
            return {
                ...state,
                fetchedMessage: action.payload
            }
        default:
            return state;
    }
}
