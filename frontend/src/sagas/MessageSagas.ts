import {all, call, put, select, takeEvery} from 'redux-saga/effects'
import * as url from "../config/url"
import axios from 'axios'
import {
    loading,
    messageDeleteRoutine,
    messageEditRoutine,
    messageFetchRoutine,
    messageSendRoutine,
    messagesFetchRoutine,
    prevMessageRoutine
} from "../actions/ActionTypes";
import {push} from "connected-react-router";

export function* fetchMessages() {
    yield put({type: loading.TRIGGER});

    try {
        const messages = yield call(axios.get, `${url.api}/message/`);
        console.log(messages);
        yield put({type: messagesFetchRoutine.SUCCESS, payload: {messages: messages.data}});

        yield put({type: prevMessageRoutine.REQUEST})
        yield put({type: loading.SUCCESS});
    } catch (e) {
        yield put({type: loading.FAILURE});
    }
}

function* watchFetchMessages() {
    yield takeEvery(messagesFetchRoutine.REQUEST, fetchMessages);
}

export function* sendMessage(action: any) {
    yield put({type: messageSendRoutine.TRIGGER});

    try {
        const message = yield call(axios.post, `${url.api}/message/`, action.payload);

        const messages = yield call(axios.get, `${url.api}/message/`, action.payload);
        yield put({type: messagesFetchRoutine.SUCCESS, payload: {messages: messages.data}});
        yield put({type: messagesFetchRoutine.REQUEST});

        yield put({type: messageSendRoutine.SUCCESS, payload: message});
    } catch (e) {
        yield put({type: messageSendRoutine.FAILURE});
    }
}

function* watchSendMessage() {
    yield takeEvery(messageSendRoutine.REQUEST, sendMessage);
}

export function* deleteMessage(action: any) {
    try {
        yield call(axios.delete, `${url.api}/message/${action.payload}`)
        yield put({type: messageDeleteRoutine.SUCCESS});

        yield put({type: messagesFetchRoutine.REQUEST})
    } catch (e) {
        yield put({type: messageDeleteRoutine.FAILURE});
    }
}

function* watchDeleteMessage() {
    yield takeEvery(messageDeleteRoutine.REQUEST, deleteMessage);
}

export function* editMessage(action: any) {
    yield put({type: loading.TRIGGER});

    try {
        yield call(axios.patch, `${url.api}/message/${action.payload.id}`, action.payload);
        yield put(push('/'));

        yield put({type: messageEditRoutine.SUCCESS});
        // yield put({type: messagesFetchRoutine.REQUEST})
    } catch (e) {
        yield put({type: loading.FAILURE});
    }
}

function* watchEditMessage() {
    yield takeEvery(messageEditRoutine.REQUEST, editMessage);
}

export function* fetchMessage(action: any) {
    yield put({type: loading.TRIGGER});

    try {
        const message = yield call(axios.get, `${url.api}/message/${action.payload}`)
        yield put({type: messageFetchRoutine.SUCCESS, payload: message.data})

        yield put({type: loading.SUCCESS});
    } catch (e) {
        yield put({type: loading.FAILURE});
    }
}

function* watchFetchMessage() {
    yield takeEvery(messageFetchRoutine.REQUEST, fetchMessage);
}

export function* setPrevMessage() {
    const state = yield select();
    const messages = state.chat.messages;
    const currUser = state.users.currUser;

    let prevMessage = undefined;
    for (let i = messages.length-1; i >= 0; i--) {
        if (currUser.userId === messages[i].user?.userId) {
            prevMessage = messages[i];
            break;
        }
    }
    yield put({type: prevMessageRoutine.SUCCESS, payload: prevMessage});
}

function* watchSetPrevMessage() {
    yield takeEvery(prevMessageRoutine.REQUEST, setPrevMessage);
}

export function* likeMessage(action: any) {
    yield put({type: loading.TRIGGER});

    try {
        const currUser = yield select((state => state.users.currUser));
        yield call(axios.post, `${url.api}/message/${action.payload}`, {userId: currUser.userId});

        yield put({type: messagesFetchRoutine.REQUEST});
        yield put({type: loading.SUCCESS})
    } catch (e) {
        yield put({type: loading.FAILURE})
    }
}

function* watchLikeMessage() {
    // yield takeEvery(LIKE_MESSAGE, likeMessage);
}

export default function* messageSagas() {
    yield all([
        watchFetchMessages(),
        watchSendMessage(),
        watchDeleteMessage(),
        watchEditMessage(),
        watchFetchMessage(),
        watchSetPrevMessage(),
        watchLikeMessage()
    ]);
}
