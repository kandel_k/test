import {all, call, put, takeEvery} from 'redux-saga/effects'
import {
    loading,
    loginRoutine,
    SET_CURR_USER,
    userAddRoutine,
    userDeleteRoutine,
    userEditRoutine,
    userFetchRoutine,
    usersFetchRoutine
} from "../actions/ActionTypes";
import axios from 'axios'
import * as url from '../config/url'
import {push} from 'connected-react-router'

export function* login(action: any) {
    yield put({type: loading.TRIGGER});

    try {
        const currUser = yield call(axios.post, `${url.api}/login`, action.payload);
        yield put({type: SET_CURR_USER, payload: currUser.data});

        yield put({type: loginRoutine.SUCCESS});
        yield put({type: loading.SUCCESS})

        if (currUser.data.role === 'ADMIN') {
            yield put(push('/users'));
        } else {
            yield put(push('/'));
        }
    } catch (e) {
        yield put({type: loading.FAILURE});
    }
}

function* watchLogin() {
    yield takeEvery(loginRoutine.REQUEST, login);
}

export function* fetchUsers() {
    yield put({type: loading.TRIGGER});

    try {
        const users = yield call(axios.get, `${url.api}/users`);
        yield put({type: usersFetchRoutine.SUCCESS, payload: users.data})

        yield put({type: loading.SUCCESS});
    } catch (e) {
        yield put({type: loading.FAILURE});
    }
}

function* watchFetchUsers() {
    yield takeEvery(usersFetchRoutine.REQUEST, fetchUsers);
}

export function* editUser(action: any) {
    yield put({type: loading.TRIGGER});

    try {
        yield call(axios.patch, `${url.api}/user/${action.payload.userId}`, action.payload);
        yield put(push('/users'));

        yield put({type: loading.SUCCESS});
    } catch (e) {
        yield put({type: loading.FAILURE})
    }
}

function* watchEditUser() {
    yield takeEvery(userEditRoutine.REQUEST, editUser);
}

export function* fetchUser(action: any) {
    yield put({type: loading.TRIGGER});

    try {
        const user = yield call(axios.get, `${url.api}/user/${action.payload}`);
        yield put({type: userFetchRoutine.SUCCESS, payload: user.data})

        yield put({type: loading.SUCCESS});
    } catch (e) {
        yield put({type: loading.FAILURE});
    }
}

function* watchFetchUser() {
    yield takeEvery(userFetchRoutine.REQUEST, fetchUser);
}

export function* addUser(action: any) {
    yield put({type: loading.TRIGGER});

    try {
        yield call(axios.post, `${url.api}/user/`, action.payload);
        yield put(push('/users'));

        yield put({type: loading.FAILURE});
    } catch (e) {
        yield put({type: loading.FAILURE})
    }
}

function* watchAddUser() {
    yield takeEvery(userAddRoutine.REQUEST, addUser);
}

export function* deleteUser(action: any) {
    try {
        yield call(axios.delete, `${url.api}/user/${action.payload}`)
        yield put({type: userDeleteRoutine.SUCCESS});

        yield put({type: usersFetchRoutine.REQUEST})
    } catch (e) {
        yield put({type: userDeleteRoutine.FAILURE});
    }
}

function* watchDeleteUser() {
    yield takeEvery(userDeleteRoutine.REQUEST, deleteUser);
}

export default function* userSagas() {
    yield all([
        watchLogin(),
        watchFetchUsers(),
        watchFetchUser(),
        watchEditUser(),
        watchAddUser(),
        watchDeleteUser()
    ]);
}
