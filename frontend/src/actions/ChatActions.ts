import {
    LIKE_MESSAGE,
    messageDeleteRoutine,
    messageEditRoutine,
    messageFetchRoutine,
    messageSendRoutine,
    messagesFetchRoutine,
} from "./ActionTypes";

export const fetchMessages = () => ({
    type: messagesFetchRoutine.REQUEST
})

export const fetchMessage = (data: string) => ({
    type: messageFetchRoutine.REQUEST,
    payload: data
})

export const editMessage = (data: any) => ({
    type: messageEditRoutine.REQUEST,
    payload: data
})

export const sendMessage = (data: any) => ({
    type: messageSendRoutine.REQUEST,
    payload: data
});

export const deleteMessage = (data: string) => ({
    type: messageDeleteRoutine.REQUEST,
    payload: data
})

export const messageCancelEdit = () => ({
    type: messageEditRoutine.SUCCESS
})

export const likeMessage = (data: any) => ({
    type: LIKE_MESSAGE,
    payload: data
})
