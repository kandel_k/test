import {createRoutine} from "redux-saga-routines";

export const SET_CURR_USER = 'SET_CURR_USER';
export const loginRoutine = createRoutine('LOGIN');
export const loading = createRoutine('LOADING');

export const LIKE_MESSAGE = 'LIKE_MESSAGE';
export const prevMessageRoutine = createRoutine('PREV_MESSAGE_ROUTINE');
export const messagesFetchRoutine = createRoutine('MESSAGES_FETCH');
export const messageSendRoutine = createRoutine('MESSAGE_SEND');
export const messageDeleteRoutine = createRoutine('DELETE_MESSAGE');
export const messageFetchRoutine = createRoutine('MESSAGE_FETCH');
export const messageEditRoutine = createRoutine('MESSAGE_EDIT');

export const userFetchRoutine = createRoutine('USER_FETCH');
export const userEditRoutine = createRoutine('USER_EDIT');
export const userAddRoutine = createRoutine('USER_ADD');
export const usersFetchRoutine = createRoutine('USERS_FETCH');
export const userDeleteRoutine = createRoutine('USER_DELETE');
