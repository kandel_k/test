import {
    loginRoutine,
    userAddRoutine,
    userDeleteRoutine,
    userEditRoutine,
    userFetchRoutine,
    usersFetchRoutine
} from "./ActionTypes";

// const setCurrUserAction = (data: any) => ({
//     type: SET_CURR_USER,
//     payload: data
// });

export const login = (data: any) => ({
    type: loginRoutine.REQUEST,
    payload: data
});

export const fetchUsers = () => ({
    type: usersFetchRoutine.REQUEST
});

export const fetchUser = (data: any) => ({
    type: userFetchRoutine.REQUEST,
    payload: data
});

export const editUser = (data: any) => ({
    type: userEditRoutine.REQUEST,
    payload: data
});

export const addUser = (data: any) => ({
   type: userAddRoutine.REQUEST,
   payload: data
});

export const deleteUser = (data: any) => ({
   type: userDeleteRoutine.REQUEST,
   payload: data
});

// export const setCurrUser = (currUser: UserInterface) => (dispatch: any) => {
//     dispatch(setCurrUserAction({currUser: currUser}));
// }

export const userCancelEdit = () => ({
    type: userEditRoutine.SUCCESS
})
