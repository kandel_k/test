import {UserInterface} from "../containers/Chat";

export const getDefaultUser: UserInterface = ({
    userId: '',
    avatar: '',
    user: '',
    password: ''
})
