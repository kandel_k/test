import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import ChatReducer from "./reducers/ChatReducer";
import createSagaMiddleware from "redux-saga";
import UserReducer from "./reducers/UserReducer";
import rootSaga from "./sagas";
import {createBrowserHistory} from 'history';
import {connectRouter, routerMiddleware} from 'connected-react-router'

const initialState = {};

export const history = createBrowserHistory();

const reducers = {
    router: connectRouter(history),
    chat: ChatReducer,
    users: UserReducer
}

const sagaMiddleware = createSagaMiddleware();

// @ts-ignore
const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;

const store = createStore(
    combineReducers(reducers),
    initialState,
    composeEnhancers(applyMiddleware(routerMiddleware(history), sagaMiddleware)));

sagaMiddleware.run(rootSaga);

export default store;
