import React from "react";
import {Message} from "semantic-ui-react";
import './style.scss';
import PropTypes, {InferType} from 'prop-types';
import {connect} from "react-redux";

const ErrorMessage: any = ({isLoadingError}: InferType<typeof ErrorMessage.propTypes>) => {
    return (
        <Message hidden={!isLoadingError} negative className={'error-message'}>
            <Message.Header>Oops</Message.Header>
            <p>Something went wrong. Please try again later</p>
        </Message>
    );
}

ErrorMessage.propTypes = {
    isLoadingError: PropTypes.bool
}

const mapStateToProps = (state: any) => ({
    isLoadingError: state.chat.isLoadingError
});

export default connect(
    mapStateToProps,
    null
)(ErrorMessage);
