import React, {useState} from "react";
import {Button, Form, Grid, Header, Message, Segment} from "semantic-ui-react";
import PropTypes, {InferType} from 'prop-types'
import {connect} from "react-redux";
import {login} from '../../actions/UserActions'

const LoginPage: any = ({login, isError, isLoading}: InferType<typeof LoginPage.propTypes>) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const nameChanged = (event: any) => {
        setUsername(event.target.value);
    };

    const passwordChanged = (event: any) => {
        setPassword(event.target.value);
    };

    const handleLoginClick = () => {
        login({username, password});
    }

    return (
        <Grid textAlign='center' style={{height: '100vh'}} verticalAlign='middle'>
            <Grid.Column style={{maxWidth: 450}}>
                <Header as='h2' color='teal' style={{display:'flex', justifyContent: 'center'}}>
                    Log-in to your account
                </Header>
                <Form size='large' error={isError}>
                    <Segment stacked>
                        <Form.Input
                            fluid icon='user'
                            iconPosition='left'
                            placeholder='Username'
                            onChange={nameChanged}/>
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                            onChange={passwordChanged}
                        />
                        <Button color='teal' fluid size='large' loading={isLoading} onClick={handleLoginClick}>
                            Login
                        </Button>
                    </Segment>
                    <Message error>
                        Email or password is incorrect
                    </Message>
                </Form>
            </Grid.Column>
        </Grid>
    );
}

LoginPage.propTypes = {
    login: PropTypes.func.isRequired,
    isError: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired
}

const mapStateToProps = ({chat}: any) => ({
    isLoading: chat.isLoading,
    isError: chat.isLoadingError
})

const actions = {login}

export default connect(
    mapStateToProps,
    actions
)(LoginPage);
