import React from "react";
import {Button, ButtonGroup, Item, ItemContent} from "semantic-ui-react";
import noAvatar from "../../resources/img/noavatar.png";
import PropTypes, {InferType} from 'prop-types'
import {deleteUser} from "../../actions/UserActions";
import {history} from "../../store";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

const User: any = ({user, deleteUser}: InferType<typeof User.propTypes>) => {

    const onEdit = () => {
        history.push(`/user/${user.userId}`)
    }

    const onDelete = () => {
        deleteUser(user.userId);
    }

    return (
        <div className={'user-item'}>
            <Item.Image size={"tiny"} src={(user.avatar.length > 0) ? user.avatar : noAvatar} />

            <ItemContent >
                <Item.Header>{user.user}</Item.Header>
                <ButtonGroup>
                    <Button onClick={onDelete}>Delete</Button>
                    <Button onClick={onEdit} color={'blue'}>Edit</Button>
                </ButtonGroup>
            </ItemContent>
        </div>
    );
}

User.propTypes = {
    user: PropTypes.object.isRequired,
    deleteUser: PropTypes.func.isRequired
}

const actions = {
    deleteUser
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(User);
