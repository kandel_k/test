import React, {useEffect} from "react";
import PropTypes, {InferType} from 'prop-types'
import './style.scss'
import {ItemGroup} from "semantic-ui-react";
import Message from "../../components/Message/Message";
import {countMaxHeight} from "../../services/ElementService";
import {connect} from "react-redux";
import {deleteMessage, likeMessage} from "../../actions/ChatActions";
import {bindActionCreators} from "redux";
import {history} from "../../store";

const MessageContainer: any = (
    {
        messages,
        currUser,
        deleteMessage,
        likeMessage,
        prevMessage
    }: InferType<typeof MessageContainer.propTypes>
) => {

    const handleKeyDown = (event: any) => {
        if (event.key === 'ArrowUp') {
            if (prevMessage) {
                history.push(`/message/${prevMessage.id}`)
            }
            event.preventDefault();
        }
    }

    useEffect(() => {
        let element = document.getElementsByClassName('message-container')[0] as HTMLElement;
        element.style.maxHeight = countMaxHeight() + 'px';
    }, [])

    return (
        <div className={'message-container'} onKeyDown={handleKeyDown} tabIndex={0}>
            <ItemGroup className={'message-group'} >

                {messages.map((message: object, i: number) => {
                    return <Message key={i} message={message} currUser={currUser} deleteMessage={deleteMessage}
                                    likeMessage={likeMessage}/>
                })}

            </ItemGroup>
        </div>
    );
}

MessageContainer.propTypes = {
    messages: PropTypes.arrayOf(Object).isRequired,
    prevMessage: PropTypes.object,
    currUser: PropTypes.object.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired
}

MessageContainer.defaultProps = {
    messages: []
}

const mapStateToProps = (state: any) => ({
    messages: state.chat.messages,
    currUser: state.users.currUser,
    prevMessage: state.chat.prevMessage
});

const actions = {
    deleteMessage,
    likeMessage
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageContainer);
