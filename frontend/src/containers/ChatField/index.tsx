import React from "react";
import MessageForm from "../../components/SendMessageForm/MessageForm";
import './style.scss'
import MessageContainer from "../MessageContainer";

const ChatField = () => {

    return (
        <div className={'chat-field'}>
            <MessageContainer />
            <MessageForm />
        </div>
    );
}

export default ChatField
