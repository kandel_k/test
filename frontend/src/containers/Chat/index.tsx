import React, {useEffect} from 'react'
import Header from "../Header";
import './style.scss'
import Main from "../Main";
import {Dimmer, Loader} from "semantic-ui-react";
import {fetchMessages} from "../../actions/ChatActions";
import PropTypes, {InferProps} from 'prop-types'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Redirect} from "react-router";
import {fetchUsers} from "../../actions/UserActions";
import ErrorMessage from "../../components/Error";

export interface UserInterface {
    userId: string,
    avatar: string,
    user: string,
    password?: string
}

export interface MessageInterface {
    id?: string,
    userId: string,
    user?: string,
    text: string,
    isLiked?: boolean,
    createdAt?: Date,
    editedAt?: string
}

const Chat: any = ({isLoading, fetchMessages, isAuthorized, fetchUsers }: InferProps<typeof Chat.propTypes>) => {

    useEffect(() => {
        if (isAuthorized) {
            fetchMessages();
            fetchUsers();
        }
    }, []);

    const showLoading = () => {
        return (
            <Dimmer active>
                <Loader>Loading</Loader>
            </Dimmer>
        )
    }

    const showChat = () => {
        return (
            <div className={'container'}>
                <Header />
                <Main />
                <ErrorMessage/>
            </div>
        );
    }

    return (
        !isAuthorized
            ? <Redirect to={'/login'} />
            : (isLoading && showLoading()) || showChat()
    );
}

Chat.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    fetchMessages: PropTypes.func.isRequired,
    isAuthorized: PropTypes.bool.isRequired,
    fetchUsers: PropTypes.func.isRequired
}

const mapStateToProps = (state:any) => ({
    isLoading: state.chat.isLoading,
    isAuthorized: state.users.isAuthorized
});

const actions = {
    fetchUsers,
    fetchMessages
}

const mapDispatchToProps = (dispatch: any):any => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);
