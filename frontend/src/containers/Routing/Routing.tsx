import React from "react";
import {Route, Switch} from "react-router";
import Chat from "../Chat";
import EditMessageForm from "../../components/EditMessageForm";
import LoginPage from "../../components/Login/LoginPage"
import UserList from "../UserList";
import EditUserForm from "../../components/EditUserForm";

const Routing = () => {

    return (
        <Switch>
            <Route exact path={'/'} component={Chat}/>
            <Route exact path={'/login'} component={LoginPage}/>
            <Route path={'/message/:id'} component={EditMessageForm}/>
            <Route exact path={'/users'} component={UserList}/>
            <Route exact path={['/user/:id', '/user/']} component={EditUserForm}/>
        </Switch>
    );
}

export default Routing
