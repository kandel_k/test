import React from 'react'

import './style.scss'
import HeaderLogo from "../../components/HeaderLogo";
import ChatHeader from "../../components/ChatHeader";

const Header = () => {

    return (
        <header>
            <div className='wrapper header'>
                <HeaderLogo/>
                <ChatHeader />
            </div>
        </header>
    );
}

export default Header;
