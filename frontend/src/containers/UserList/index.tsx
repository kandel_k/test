import React, {useEffect} from "react";
import PropTypes, {InferType} from 'prop-types';
import {connect} from "react-redux";
import User from "../../components/User";
import {Button, Dimmer, Loader} from "semantic-ui-react";
import {Redirect} from "react-router";
import {fetchUsers} from "../../actions/UserActions";
import {bindActionCreators} from "redux";
import {history} from "../../store";
import './style.scss';
import ErrorMessage from "../../components/Error";

const UserList: any = ({users, isLoading, isAuthorized, currUser, fetchUsers}: InferType<typeof UserList.propTypes>) => {

    const onAdd = () => {
        history.push('/user/');
    }

    useEffect(() => {
        if (isAuthorized) {
            fetchUsers();
        }
    }, [])

    const showUsers = () => (
        <div className={'user-container'}>
            <Button color={'green'} className={'add-user-btn'} onClick={onAdd}>Add new</Button>
            <div className={'user-list'}>
                {/*@ts-ignore*/}
                {users.map((user: any) => <User key={user.userId} user={user}/>)}
            </div>
            <ErrorMessage/>
        </div>
    );

    const showLoading = () => {
        return (
            <Dimmer active>
                <Loader>Loading</Loader>
            </Dimmer>
        )
    }

    return (
        (!isAuthorized || currUser.role !== 'ADMIN')
            ? <Redirect to={'/login'} />
            : (isLoading ? showLoading() : showUsers())
    )
}

UserList.propTypes = {
    users: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isAuthorized: PropTypes.bool.isRequired,
    currUser: PropTypes.object.isRequired,
    fetchUsers: PropTypes.func.isRequired
}

const mapStateToProps = (state: any) => ({
    users: state.users.users,
    isLoading: state.chat.isLoading,
    isAuthorized: state.users.isAuthorized,
    currUser: state.users.currUser
})

const actions = {
    fetchUsers
};

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserList);
