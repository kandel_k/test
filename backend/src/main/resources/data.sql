INSERT INTO users (user_id, avatar, user) VALUES
('9e243930-83c9-11e9-8e0c-8f1a686f4ce4', 'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA', 'Ruth'),
('533b5230-1b8f-11e8-9629-c7eca82aa7bd', 'https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng', 'Wendy'),
('4b003c20-1b8f-11e8-9629-c7eca82aa7bd', 'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ', 'Helen');

INSERT INTO users(user_id, avatar, user, password, role) VALUES
('4b003c20-1b8f-11e8-9629-c7eca84521sd', '', 'admin', 'admin', 'ADMIN');

INSERT INTO messages (id, text, user_id) VALUES
('80f08600-1b8f-11e8-9629-c7eca82aa7bd', 'I don’t *** understand. It''s the Panama accounts', '9e243930-83c9-11e9-8e0c-8f1a686f4ce4'),
('80e00b40-1b8f-11e8-9629-c7eca82aa7bd', 'Tells exactly what happened.', '533b5230-1b8f-11e8-9629-c7eca82aa7bd'),
('80e03259-1b8f-11e8-9629-c7eca82aa7bd', 'You were doing your daily bank transfers and…', '533b5230-1b8f-11e8-9629-c7eca82aa7bd'),
('80e03256-1b8f-11e8-9629-c7eca82aa7bd', 'Why this account?', '4b003c20-1b8f-11e8-9629-c7eca82aa7bd');
