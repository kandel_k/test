package org.binary.chat.Service;

import org.binary.chat.Entity.UserEntity;
import org.binary.chat.Entity.dto.UserLoginRequest;
import org.binary.chat.Repository.MessageRepository;
import org.binary.chat.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    public List<UserEntity> getAll() {
        return userRepository.findAll();
    }

    public Optional<UserEntity> login(UserLoginRequest loginRequest) {
        return userRepository.findByUserAndPassword(loginRequest.username, loginRequest.password);
    }

    public Optional<UserEntity> fetchUser(String id) {
        return userRepository.findById(id);
    }

    public Optional<UserEntity> addUser(UserEntity userEntity) {
        if (userRepository.findByUser(userEntity.getUser()).isPresent()) {
            return Optional.empty();
        }
        return Optional.of(userRepository.save(userEntity));
    }

    public void deleteUser(String id) {
        messageRepository.deleteUserFromMessages(id);
        userRepository.deleteById(id);
    }

    public Optional<UserEntity> updateUser(String id, UserEntity userEntity) {
        var user = userRepository.findById(id);
        return user.map(val -> {
            val = userEntity;
            return userRepository.save(val);
        });
    }
}
