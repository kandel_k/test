package org.binary.chat.Service;

import org.binary.chat.Entity.MessageEntity;
import org.binary.chat.Entity.dto.SendMessage;
import org.binary.chat.Repository.MessageRepository;
import org.binary.chat.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    public List<MessageEntity> getAll() {

        return messageRepository.findAll();
    }

    public MessageEntity sendMessage(SendMessage message) {
        var user = userRepository.findById(message.userId);
        return messageRepository.save(new MessageEntity(message.text, user.get()));
    }

    public void deleteMessage(String id) {
        messageRepository.deleteById(id);
    }

    public Optional<MessageEntity> getMessage(String id) {
        return messageRepository.findById(id);
    }

    public Optional<MessageEntity> updateMessage(String id, String text) {
        var message = messageRepository.findById(id);
        return message.map(mes -> {
            mes.setText(text);
            return messageRepository.save(mes);
        });
    }
}
