package org.binary.chat.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.binary.chat.Config.Auditable;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "messages")
@NoArgsConstructor
public class MessageEntity extends Auditable<String> {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column
    private String text;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    public MessageEntity(String text, UserEntity user) {
        this.text = text;
        this.user = user;
    }
}
