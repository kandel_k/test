package org.binary.chat.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String userId;

    @Column
    private String avatar;

    @Column
    private String user;

    @Column(columnDefinition = "varchar(50) default 'USER'")
    private String role;

    @Column(columnDefinition = "varchar(255) default '123'")
    private String password;
}
