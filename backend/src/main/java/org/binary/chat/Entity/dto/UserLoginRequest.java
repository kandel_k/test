package org.binary.chat.Entity.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserLoginRequest {
    public String username;
    public String password;
}
