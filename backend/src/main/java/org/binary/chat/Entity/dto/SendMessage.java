package org.binary.chat.Entity.dto;

import lombok.ToString;

@ToString
public class SendMessage {
    public String userId;
    public String text;
}
