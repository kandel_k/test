package org.binary.chat.Entity.dto;

public class CreateUserRequest {
    public String userId;
    public String user;
    public String avatar;
    public String password;
    public String role;
}
