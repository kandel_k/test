package org.binary.chat.Controller;

import org.binary.chat.Entity.MessageEntity;
import org.binary.chat.Entity.UserEntity;
import org.binary.chat.Entity.dto.UserLoginRequest;
import org.binary.chat.Repository.MessageRepository;
import org.binary.chat.Service.MessageService;
import org.binary.chat.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserEntity>> getUsers() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping("/login")
    public ResponseEntity<UserEntity> login(@RequestBody UserLoginRequest userLoginRequest) {
        var user  = userService.login(userLoginRequest);

        return user.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UserEntity> fetchUser(@PathVariable String id) {
        return userService.fetchUser(id).map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/user/")
    public ResponseEntity<UserEntity> addUser(@RequestBody UserEntity userEntity) {
        return userService.addUser(userEntity).map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable String id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/user/{id}")
    public ResponseEntity<UserEntity> updateUser(@PathVariable String id, @RequestBody UserEntity userEntity) {
        var message = userService.updateUser(id, userEntity);
        return message.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
