package org.binary.chat.Controller;

import org.binary.chat.Entity.MessageEntity;
import org.binary.chat.Entity.dto.SendMessage;
import org.binary.chat.Service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping("/")
    public ResponseEntity<List<MessageEntity>> getMessages() {
        return ResponseEntity.ok(messageService.getAll());
    }

    @PostMapping("/")
    public ResponseEntity<MessageEntity> sendMessage(@RequestBody SendMessage sendMessage) {
        return ResponseEntity.ok(messageService.sendMessage(sendMessage));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable String id) {
        messageService.deleteMessage(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageEntity> fetchMessage(@PathVariable String id) {
        var message = messageService.getMessage(id);
        return message.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<MessageEntity> updateMessage(@PathVariable String id, @RequestBody MessageEntity request) {
        var message = messageService.updateMessage(id, request.getText());
        return message.map(ResponseEntity::ok).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

//    @PostMapping("/{id}")
//    public void likeMessage(@PathVariable String id, @RequestBody String userId) {
//        messageService.likeMessage(userId, id);
//    }
}
