package org.binary.chat.Repository;

import org.binary.chat.Entity.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, String> {

    // Dont want to play with cascade types, so wrote this method
    @Transactional
    @Modifying
    @Query("UPDATE MessageEntity messages " +
            "SET messages.user = NULL " +
            "WHERE messages.id IN (SELECT mes.id FROM MessageEntity mes WHERE mes.user.userId = :id)")
    public void deleteUserFromMessages(@Param("id") String id);
}
