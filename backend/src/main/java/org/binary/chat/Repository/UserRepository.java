package org.binary.chat.Repository;

import org.binary.chat.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {

    Optional<UserEntity> findByUserAndPassword(String username, String password);

    Optional<UserEntity> findByUser(String username);
}
